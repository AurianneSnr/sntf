package tps.ws.deployment.Services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost; 
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


public class ServiceTrain {
	
	
	
	public ServiceTrain(){
		
	}
	
	public String executeGetAllTrains() {
		try {
		URL yahoo = new URL("http://localhost:8081/trains/available");
        URLConnection yc = yahoo.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                yc.getInputStream()));
        String inputLine;
        
        List<String> trains= new ArrayList<>();
        while ((inputLine = in.readLine()) != null) 
        	trains.add(inputLine);
            System.out.println(inputLine);
        in.close();
        return trains.toString();
		}catch(Exception e){
			return "No train found.";
		}
	}
		
	public String executeGetSpecificTrain(int id) {
		CreateRequest createRequest= new CreateRequest();
		String url= "http://localhost:8081/trains/";
		return createRequest.executeGet(url, id);
		
	}
	
	public String executeGetWantedTrain(String departureStation, String returnStation, int nbPlaces, String classeTrain, String dateDeparture) {
		try {
			URL url = new URL("http://localhost:8081/trains/find");
			HttpURLConnection http = (HttpURLConnection)url.openConnection();
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			http.setRequestProperty("Authorization", "Bearer mt0dgHmLJMVQhvjpNXDyA83vA_PxH23Y1");
			http.setRequestProperty("Content-Type", "application/json");

			
			String data= "{\r\n"
					+ "    \r\n"
					+ "    \"departureStation\": \""+departureStation+ "\",\r\n"
					+ "    \"returnStation\": \""+ returnStation +"\",\r\n"
					+ "    \"dateDeparture\": \"" +dateDeparture+"\",\r\n"
					+ "    \"numberAvailableSeat\": \""+nbPlaces+"\",\r\n"
					+ "    \"classeTrain\": \""+classeTrain+"\"\r\n"
					+ "\r\n"
					+ "}";
			System.out.println(data);
			
			/*
			String data= "{\r\n"
					+ "    \r\n"
					+ "    \"departureStation\": \"Paris\",\r\n"
					+ "    \"returnStation\": \"Lyon\",\r\n"
					+ "    \"dateDeparture\": \"Fri Feb 04 10:00:00 CET 2022\",\r\n"
					+ "    \"numberAvailableSeat\": 1,\r\n"
					+ "    \"classeTrain\": \"SECOND\"\r\n"
					+ "\r\n"
					+ "}";*/
			
		
			
			
			byte[] out = data.getBytes(StandardCharsets.UTF_8);

			OutputStream stream = http.getOutputStream();
			stream.write(out);
			
			BufferedReader in = new BufferedReader(
                    new InputStreamReader(
            http.getInputStream()));
			String inputLine;

			List<String> trains= new ArrayList<>();
			while ((inputLine = in.readLine()) != null) 
				trains.add(inputLine);
				System.out.println(inputLine);
			in.close();
			
			System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
			http.disconnect();
			return trains.toString();
			}catch(Exception e) {
				return "Impossible to find a train corresponding to your search.";
			}
		}
	
	public String executePostTrain() {
		//Reservation reservation= new Reservation(idTrain, idPerson, nbPlaces);
		try {
			URL url = new URL("http://localhost:8081/train/new");
			HttpURLConnection http = (HttpURLConnection)url.openConnection();
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			http.setRequestProperty("Authorization", "Bearer mt0dgHmLJMVQhvjpNXDyA83vA_PxH23Y");
			http.setRequestProperty("Content-Type", "application/json");

			//String data = "{\n  \"idTrain\":"+ reservation.getIdTrain() + ",\n  \"idUser\":" + reservation.getIdUser() + ",\n  \"nbPlaces\": 1,\n}";
			//String data = "{\n  \"idTrain\": 1,\n  \"idUser\": \"1\",\n  \"nbPlaces\": 3,\n}";
			String data= "{\r\n"
					+ "    \r\n"
					+ "    \"departureStation\": \"Paris\",\r\n"
					+ "    \"returnStation\": \"Lyon\",\r\n"
					+ "    \"numberAvailableSeat\": \"30\",\r\n"
					+ "    \"classeTrain\": \"SECOND\"\r\n"
					+ "\r\n"
					+ "}";
			
			byte[] out = data.getBytes(StandardCharsets.UTF_8);

			OutputStream stream = http.getOutputStream();
			stream.write(out);
			BufferedReader in = new BufferedReader(
                    new InputStreamReader(
            http.getInputStream()));
			String inputLine;

			List<String> trains= new ArrayList<>();
			while ((inputLine = in.readLine()) != null) 
				trains.add(inputLine);
				System.out.println(inputLine);
			in.close();

			System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
			http.disconnect();
			return trains.toString();
			}catch(Exception e) {
				return "Your reservation can't be confirmed.";
			}
		}
	
}

