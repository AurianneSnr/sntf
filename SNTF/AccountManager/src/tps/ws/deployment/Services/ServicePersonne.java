package tps.ws.deployment.Services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import tps.ws.deployment.Model.Personne;

public class ServicePersonne {
	
	private List<Personne>allUser;
	
	public ServicePersonne(List<Personne>allUser){
		this.allUser=allUser;
	}
	
	public Personne getPersonne(String username, String mdp){
		if(username==null || mdp==null) {
			System.out.println("Login ou mot de passe incomplet");
			return null;
		}
		else { 
					//Connect connexionDB= new Connect();
                    //List<Personne>allUser= connexionDB.connect();
                    List<Personne>finalList= new ArrayList<>();
                    for(Personne personne:allUser){
                        if (personne.getLogin().equals(username)&& personne.getMdp().equals(mdp)){
                            finalList.add(personne);
                        }
                   
                    }
                    if (finalList.size()==0) {
                    	System.out.println("La liste d'utilisateur correspondant est vide");
                    	return null;
                    }
                    System.out.println(finalList.get(0));
                    
                    return finalList.get(0);
		}

}
}
	

