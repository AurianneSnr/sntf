package tps.ws.deployment.Model;

public class Reservation {
    private int id;
    private int idTrain;
    private int idUser;
    private int nbPlaces;


    public Reservation() {
    }

    public Reservation(int idTrain, int idUser, int nbPlaces) {
        this.idTrain = idTrain;
        this.idUser = idUser;
        this.nbPlaces = nbPlaces;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTrain() {
        return idTrain;
    }

    public void setIdTrain(int idTrain) {
        this.idTrain = idTrain;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getNbPlaces() {
        return nbPlaces;
    }

    public void setNbPlaces(int nbPlaces) {
        this.nbPlaces = nbPlaces;
    }
}