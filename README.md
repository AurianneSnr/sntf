# SNTF

#Développeurs
Aurianne SANNIER et Jean-Max Louison

#Description
Ce projet a pour but de créer un processus de réservation d'un voyage en train.
Il comprend quatre fonctions basiques:
-la recherche de tous les trains disponibles c'est-a-dire dont le nombre de places disponible est supérieur à 0
-la recherche de trains selon les critères suivants: station de départ, station d'arrivée, date de départ du voyage, classe du train
-la réservation d'un train
-l'authentification de l'utilisateur qui retourne sa liste de réservations

Il utilise deux services: Soap(localhost:8080) et Rest(localhost:8081).
Dans l'API Rest sont gérés les bases de donnée des trains et des réservations en H2 in memory.
Les utilisateurs sont gérés dans une liste statique dans l'API Soap.

L'API soap appelle l'api rest pour récupérer les trains, effectuer les réservations et récupérer les réservations selon l'id de l'utilisateur à sa connexion.

#Installation
Cloner le project en local, lancer AccountManager depuis eclipse et rest_project sur intellij.
Pour lancer rest_project, lancer Application.
Pour lancer AccountManager, click droit sur Account=> Run As server, démarrer un serveur et le projet puis tester les 4 fonctionnalités ci-dessous.

#Utilisation
Pour utiliser les 4 fonctionnalités (authentification, rechercher les trains, rechercher un train, réserver):
http://localhost:8080/AccountManager/services/Account/authentificate?username=lola&mdp=mdp

http://localhost:8080/AccountManager/services/Account/rechercher

http://localhost:8080/AccountManager/services/Account/rechercherTrain?departureStation=Paris&returnStation=Lyon&nbPlaces=1&classeTrain=SECOND&dateDeparture=Fri%20Feb%2004%2010:00:00%20CET%202022

http://localhost:8080/AccountManager/services/Account/reserver?idTrain=1&idPerson=1&nbPlaces=2


#   Requirements                                                                    Marks (25)
1    Create REST Train Filtering service B :                                            6/6
2    Create SOAP Train Booking service A  :                                             4/4
3    Interaction between two services :                                                 4/4
4    Test with Web service Client (instead of using Eclipse's Web service Explorer) :   0/2
5    Work with complex data type (class, table, etc.) :                                 2/2
6    Work with database (in text file, xml, in mysql, etc.) :                           1/2
7    Postman (uniquement les tests) :                                                  0,5/2
8    OpenAPI :                                                                          3/3

TOTAL                                                                                 20,5/25

Pour accéder à OpenAPI: http://localhost:8081/swagger-ui/#/
