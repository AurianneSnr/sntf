package booking.Repository;

import booking.Model.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainRepository extends JpaRepository<Train,Long> {
    //rechercher un train specifique
    List<Train> findTrainsByClasseTrainAndDepartureStationAndReturnStationAndDateDeparture(String classeTrain, String departureStation,String returnStation, String dateDeparture);
}
