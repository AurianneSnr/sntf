package booking.Services;

import booking.Controller.TrainController;
import booking.Model.Reservation;
import booking.Model.Train;
import booking.Repository.TrainRepository;

public class ServiceTrain {


    public Reservation reserver(TrainRepository trainRepository, Long idTrain, int nbPlaces, long idUser) {

        TrainController trainController= new TrainController(trainRepository);
        //on crée un nouveau train pour le modifier
        Train newTrain=trainController.oneTrain(idTrain);
        // On vérifie qu'il reste des places disponibles pour le train
        if (newTrain.getNumberAvailableSeat() >= nbPlaces) {
            //on modifie le nombre de places disponibles du nouveau train avec son nombre initial moins le nombre de places à réserver
            newTrain.setNumberAvailableSeat(newTrain.getNumberAvailableSeat() - nbPlaces);
            //on remplace à l'id du train le nouveau train avec le nouveau nombre de places
            trainController.replaceTrain(newTrain,idTrain);
            System.out.println("Réservation en départ de " + newTrain.getDepartureStation() + " à destination de " + newTrain.getReturnStation() + " le " + newTrain.getDateDeparture() + " en classe " + newTrain.getClasseTrain() + " a bie ete reserve.");
            //on crée une nouvelle réservation avec les données et on la retourne
            Reservation newR= new Reservation(idTrain,idUser,nbPlaces);
            return newR;
        } else {
            System.out.println("Aucune place disponible");
            //si aucune place n'est disponible on retourne aucune reservation
            return null;
        }

    }


}
