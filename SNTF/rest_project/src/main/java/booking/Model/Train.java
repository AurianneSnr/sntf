package booking.Model;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Time;
import java.util.Date;

@Entity
public class Train {
    private String departureStation;
    private String returnStation;
    private String dateDeparture;
    private int numberAvailableSeat;
    private @Id @GeneratedValue Long id;
    private String classeTrain;
    public enum ClasseTrain{FIRST,SECOND,BUSINESS};


    public Train(String departureStation, String returnStation,  int numberAvailableSeat, ClasseTrain classeTrain) {
        this.departureStation = departureStation;
        this.returnStation = returnStation;
        this.numberAvailableSeat = numberAvailableSeat;
        this.classeTrain=classeTrain.toString();
    }
    public Train(String dateDeparture,String departureStation, String returnStation,  int numberAvailableSeat, ClasseTrain classeTrain) {
        this.departureStation = departureStation;
        this.returnStation = returnStation;
        this.numberAvailableSeat = numberAvailableSeat;
        this.classeTrain=classeTrain.toString();
        this.dateDeparture=dateDeparture;
    }

    public Train(String departureStation, String returnStation,int numberAvailableSeat) {
        this.departureStation = departureStation;
        this.returnStation = returnStation;
        this.numberAvailableSeat = numberAvailableSeat;
    }

    public Train (String dateDeparture){
        this.dateDeparture=dateDeparture;
    }

    public Train(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClasseTrain() {
        return classeTrain;
    }

    public void setClasseTrain(String classeTrain) {
        this.classeTrain = classeTrain;
    }

    public Train(String departureStation, String returnStation) {
        this.departureStation = departureStation;
        this.returnStation = returnStation;
    }

    public String getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(String departureStation) {
        this.departureStation = departureStation;
    }

    public String getReturnStation() {
        return returnStation;
    }

    public void setReturnStation(String returnStation) {
        this.returnStation = returnStation;
    }

    public String getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(String dateDeparture) {
        this.dateDeparture = dateDeparture;
    }


    public int getNumberAvailableSeat() {
        return numberAvailableSeat;
    }

    public void setNumberAvailableSeat(int numberAvailableSeat) {
        this.numberAvailableSeat = numberAvailableSeat;
    }
}


