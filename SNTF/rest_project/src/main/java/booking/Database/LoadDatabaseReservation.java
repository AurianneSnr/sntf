package booking.Database;

import booking.Repository.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

public class LoadDatabaseReservation {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabaseReservation.class);

    @Bean
    CommandLineRunner initDatabaseReservation(ReservationRepository repository) {

        return args -> {
        };
    }

    public static class ReservationNotFoundException extends RuntimeException{
        public ReservationNotFoundException(Long id){
            super("Could not find reservation " + id);
        }

    }
}
