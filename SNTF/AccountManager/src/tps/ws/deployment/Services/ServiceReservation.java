package tps.ws.deployment.Services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ServiceReservation {
	
	public String executeGetReservation(int id) {
		try {
			URL yahoo = new URL("http://localhost:8081/reservation/"+id);
	        URLConnection yc = yahoo.openConnection();
	        BufferedReader in = new BufferedReader(
	                                new InputStreamReader(
	                                yc.getInputStream()));
	        String inputLine;
	        
	        List<String> trains= new ArrayList<>();
	        while ((inputLine = in.readLine()) != null) 
	        	trains.add(inputLine);
	            System.out.println(inputLine);
	        in.close();
	        return trains.toString();
			}catch(Exception e){
				return "erreur";
			}
		
	}
	
	public String executePostR(int idTrain, int idPerson, int nbPlaces) {
	
		try {
			URL url = new URL("http://localhost:8081/reservation/new");
			HttpURLConnection http = (HttpURLConnection)url.openConnection();
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			http.setRequestProperty("Authorization", "Bearer mt0dgHmLJMVQhvjpNXDyA83vA_PxH23Y");
			http.setRequestProperty("Content-Type", "application/json");

			//String data = "{\n  \"idTrain\":"+ reservation.getIdTrain() + ",\n  \"idUser\":" + reservation.getIdUser() + ",\n  \"nbPlaces\": 1,\n}";
			String data = "{\r\n"
					+ "    \r\n"
					+ "    \"idTrain\":" +idTrain+ ",\r\n"
					+ "    \"idUser\": "+idPerson+",\r\n"
					+ "    \"nbPlaces\":"+nbPlaces+"\r\n"
					+ "\r\n"
					+ "}";
			
			byte[] out = data.getBytes(StandardCharsets.UTF_8);
			System.out.println(out);
			

			OutputStream stream = http.getOutputStream();
			stream.write(out);
			System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
			http.disconnect();
			return http.getResponseMessage();
			}catch(Exception e) {
				return "error";
			}
	}
	
	
	
}
