package booking.Controller;

import booking.Model.Reservation;
import booking.Repository.ReservationRepository;
import booking.Services.ServiceTrain;
import booking.Repository.TrainRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ReservationController {
    @Autowired
    private final ReservationRepository repository;
    private final TrainRepository trainRepository;


    public ReservationController(ReservationRepository repository, TrainRepository trainRepository) {
        this.repository = repository;
        this.trainRepository=trainRepository;
    }

    //on recupere toutes les reservations peu importe l'utilisateur
    @GetMapping("/reservation")
    List<Reservation> all() {
        return repository.findAll();
    }


    //on recupere toutes les reservations d'un utilisateur
    @GetMapping("/reservation/{idUser}")
    List<Reservation> allForUser(@PathVariable Long idUser){
        List<Reservation> listR= repository.findAll();
        List<Reservation> listF= new ArrayList<>();
        for(Reservation R :listR) {
            if(R.getIdUser()==idUser){
                listF.add(R);
            }
        }
    return listF;
    }

    //creation d'une reservation
    @PostMapping("/reservation/new")
    public Reservation createReservation(@RequestBody Reservation newReservation){
        ServiceTrain serviceTrain=new ServiceTrain();
         newReservation= serviceTrain.reserver(trainRepository,newReservation.getIdTrain(),newReservation.getNbPlaces(),newReservation.getIdUser());
         if(newReservation==null){
             return null;
         }
         return repository.save(newReservation);
    }

    //second moyen de créer une réservation
    @PostMapping("/reservation/{idTrain}/{idUser}/{nbPlaces}")
    Reservation createReservation (@PathVariable Long idTrain, @PathVariable Long idUser, @PathVariable int nbPlaces){

        return repository.save(new Reservation(idTrain,idUser,nbPlaces));
    }
}
