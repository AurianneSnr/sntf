package booking.Database;
import booking.Model.Train;
import booking.Repository.TrainRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Calendar;

import static booking.Model.Train.ClasseTrain.*;


@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabaseTrain(TrainRepository repository) {
        Calendar c= Calendar.getInstance();
        c.set(2022, 01, 04,10,00,00);
        Train t= new Train(c.getTime().toString());
        return args -> {
           log.info("Preloading " + repository.save(new Train("Fri Feb 04 10:00:00 CET 2022","Paris","Lyon",80,SECOND)));
           log.info("Preloading " + repository.save(new Train("Fri Feb 04 10:00:00 CET 2022","Paris","Marseille",80,BUSINESS)));
            log.info("Preloading " + repository.save(new Train("Fri Feb 04 10:00:00 CET 2022","Paris","Marseille",60,SECOND)));
            log.info("Preloading " + repository.save(new Train("Fri Feb 04 10:00:00 CET 2022","Paris","Marseille",0,FIRST)));
            log.info("Preloading " + repository.save(new Train("Fri Feb 04 10:00:00 CET 2022","Paris","Marseille",70,SECOND)));
            log.info("Preloading " + repository.save(new Train("Fri Feb 04 10:00:00 CET 2022","Paris","Marseille",10,BUSINESS)));
        };
    }

    public static class TrainNotFoundException extends RuntimeException{
        public TrainNotFoundException(Long id){
            super("Could not find train " + id);
        }

    }
}