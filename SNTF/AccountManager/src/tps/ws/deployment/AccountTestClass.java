package tps.ws.deployment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tps.ws.deployment.Services.ServicePersonne;
import tps.ws.deployment.Services.ServiceReservation;
import tps.ws.deployment.Services.ServiceTrain;
import tps.ws.deployment.Model.Personne;
import tps.ws.deployment.Services.*;
import tps.ws.deployment.Services.*;

public class AccountTestClass {
    private String DBPath = "/C:\\Sqlite\\SNTF.db";
    private Connection connection = null;
    private Statement statement = null;
 
    public AccountTestClass() {
        
    }
    //technique de connexion base de donn�e abort�e
    public List<Personne> connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:/C:\\Sqlite\\SNTF");
            statement = connection.createStatement();
            
            String allFromUser = "select * from User";
            ResultSet result = statement.executeQuery(allFromUser);
         
            List<Personne> allUser=new ArrayList<>();
            while (result.next()) {
                int id = result.getInt("Id");
                String username = result.getString("Username");
                String password = result.getString("Password");
                String lastname = result.getString("Lastname");
                String firstname = result.getString("Firstname");
                int id1= result.getInt("id");
                Personne p=new Personne(username,password,lastname,firstname,id1);
                allUser.add(p);
                System.out.println( username + " | " + username + " | " + password + " | " + lastname + " | " + firstname );
            }
            
            System.out.println("Connexion a " + DBPath + " avec succ�s");
            return allUser;
        } catch (ClassNotFoundException notFoundException) {
            notFoundException.printStackTrace();
            System.out.println("Erreur de connexion");
            return null;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            System.out.println("Erreur de connecxion");
            return null;
        }
       
    }
    
    
    public void close() {
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //test en java application des services
    public static void main(String[] args) {
    	List<Personne>allUser= new ArrayList<>();
		Personne personne1= new Personne("lola","mdp","lolita","marcel",1);
		allUser.add(personne1);
		ServicePersonne service= new ServicePersonne(allUser);
		ServiceReservation serviceR= new ServiceReservation();
		ServiceTrain serviceT= new ServiceTrain();
		String username="lola";
		String mdp="mdp";
		if(service.getPersonne(username, mdp)==null)
		{
		System.out.println("connexion echouee");
		
		}
		else {
			System.out.println("connexion ok");
			String trains= serviceR.executeGetReservation(service.getPersonne(username, mdp).getId());
			System.out.println("Connexion r�ussie, bonjour "+ service.getPersonne(username, mdp).getSurname() + "/n Voici tous les trains "+ trains);
		}
		int idTrain=2;
		int idUser=1;
		int nbPlaces=1;
		String departureStation="Paris";
		String returnStation="Lyon";
		String classeTrain= "SECOND";
		String departureDate= "Fri Feb 04 10:00:00 CET 2022";
		
		//String nvlleResa=serviceR.executePostR(idTrain,idUser,nbPlaces);
		String wantedTrain= serviceT.executeGetWantedTrain(departureStation, returnStation, nbPlaces, classeTrain,departureDate);
		//String nvTrain= serviceT.executePostTrain();
		//System.out.println(nvTrain);
		System.out.println(wantedTrain);
    }
}


        	
        
        