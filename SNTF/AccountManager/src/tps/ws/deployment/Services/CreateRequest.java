package tps.ws.deployment.Services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost; 
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import tps.ws.deployment.Model.Reservation;
import tps.ws.deployment.Model.Train;

public class CreateRequest {
	
	public CreateRequest(){
		
	}
	
	public String executePost(String targetURL,Train train) {
		
		try {
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost(targetURL);
		String classeTrain = train.getClasseTrain();
	    String departureStation = train.getDepartureStation();
	    String returnStation= train.getReturnStation();
	    String departureDate= train.getDateDeparture().toString();
	    
		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(3);
		params.add(new NameValuePair("returnStation", returnStation));
		params.add(new NameValuePair("classeTrain", classeTrain));
		params.add(new NameValuePair("departureStation", departureStation));
		httppost.setEntity(new UrlEncodedFormEntity((List<? extends org.apache.http.NameValuePair>) params, "UTF-8"));

		//Execute and get the response.
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity entity = response.getEntity();

		if (entity != null) {
		    InputStream instream = entity.getContent();
		        return instream.toString();
		    }
		return null;
		}catch(Exception e){
			return "error";
		}
		}
	
public String executePostReservation(String targetURL,Reservation reservation) {
	
	try {
	URL url = new URL(targetURL);
	HttpURLConnection http = (HttpURLConnection)url.openConnection();
	http.setRequestMethod("POST");
	http.setDoOutput(true);
	//http.setRequestProperty("Authorization", "Bearer mt0dgHmLJMVQhvjpNXDyA83vA_PxH23Y");
	http.setRequestProperty("Content-Type", "application/json");

	//String data = "{\n  \"idTrain\":"+ reservation.getIdTrain() + ",\n  \"idUser\":" + reservation.getIdUser() + ",\n  \"nbPlaces\": 1,\n}";
	String data = "{\n  \"idTrain\": 1,\n  \"idUser\": \"1\",\n  \"nbPlaces\": 1,\n}";
	
	byte[] out = data.getBytes(StandardCharsets.UTF_8);

	OutputStream stream = http.getOutputStream();
	stream.write(out);

	System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
	http.disconnect();
	return http.getResponseMessage();
	}catch(Exception e) {
		return "error";
	}
}
	
	public String executeGet(String url, int id) {
		try {
		URL yahoo = new URL(url+id);
        URLConnection yc = yahoo.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                yc.getInputStream()));
        String inputLine;
        
        List<String> trains= new ArrayList<>();
        while ((inputLine = in.readLine()) != null) 
        	trains.add(inputLine);
            System.out.println(inputLine);
        in.close();
        return trains.toString();
		}catch(Exception e){
			return "erreur";
		}

}
	
	public String executeGetWithParameters(String targetURL, String urlParameters) {
		  
		HttpURLConnection connection = null;

		  try {
		    //Create connection
		    URL url = new URL(targetURL);
		    connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("GET");
		    connection.setRequestProperty("Content-Type", 
		        "application/x-www-form-urlencoded");

		    connection.setRequestProperty("Content-Length", 
		        Integer.toString(urlParameters.getBytes().length));
		    connection.setRequestProperty("Content-Language", "en-US");  

		    connection.setUseCaches(false);
		    connection.setDoOutput(true);

		    //Send request
		    DataOutputStream wr = new DataOutputStream (
		        connection.getOutputStream());
		    wr.writeBytes(urlParameters);
		    wr.close();

		    //Get Response  
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
		    String line;
		    while ((line = rd.readLine()) != null) {
		      response.append(line);
		      response.append('\r');
		    }
		    rd.close();
		    return response.toString();
		  } catch (Exception e) {
		    e.printStackTrace();
		    return null;
		  } finally {
		    if (connection != null) {
		      connection.disconnect();
		    }
		  }
		}

}
