package tps.ws.deployment.Model;

public class Personne {
	
	private String lastname;
	private String mdp;
	private String username;
	private String surname;
	private int id;
	
	Personne(){
		
	}
	
public Personne(String username, String mdp, String lastname, String surname, int id){
	this.username=username;
	this.mdp=mdp;
	this.surname=surname;
	this.lastname=lastname;
	this.id=id;

}


Personne(String username, String mdp){
	this.username=username;
	this.mdp=mdp;

}
	
	public String getLogin() {
        return username;
    }
	
	public int getId() {
		return id;
	}

    public void setLogin(String username) {
        this.username = username;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastName(String lastname) {
        this.lastname = lastname;
    }
    
    public String getSurname() {
        return surname;
    }

    public void setSurName(String surname) {
        this.surname = surname;
    }
    
    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
	


}
