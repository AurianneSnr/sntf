package booking.Controller;

import booking.Database.LoadDatabase;
import booking.Model.Train;
import booking.Repository.TrainRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TrainController {
    @Autowired
    private final TrainRepository repository;


    public TrainController(TrainRepository repository) {
        this.repository = repository;
    }

    //on recupere tous les trains
    @GetMapping("/trains")
    List<Train> all() {
        return repository.findAll();
    }

    //on recupere tous les trains dont le nombre de places est supeieur à 0
    @GetMapping("/trains/available")
    List<Train> allAvailableTrains() {
        List<Train> listT = repository.findAll();
        List<Train> listF = new ArrayList<>();
        for (Train T : listT) {
            if (T.getNumberAvailableSeat() > 0) {
                listF.add(T);
            }
        }
        return listF;
    }

    //on recupere tous les trains qui conviennent aux criteres de recherche
    @PostMapping("/trains/find")
    List<Train> findTrains(@RequestBody Train findTrain){
        //recherche des trains allers
        List<Train> listT =repository.findTrainsByClasseTrainAndDepartureStationAndReturnStationAndDateDeparture(findTrain.getClasseTrain(),findTrain.getDepartureStation(),findTrain.getReturnStation(),findTrain.getDateDeparture());
        Train returnTrain= new Train(findTrain.getReturnStation(),findTrain.getDepartureStation(), findTrain.getNumberAvailableSeat());
        returnTrain.setClasseTrain(findTrain.getClasseTrain());
        List<Train>listA= repository.findTrainsByClasseTrainAndDepartureStationAndReturnStationAndDateDeparture(returnTrain.getClasseTrain(),returnTrain.getDepartureStation(),returnTrain.getReturnStation(),returnTrain.getDateDeparture());
        List<Train> listF = new ArrayList<>();
        for (Train T : listT) {
            if (T.getNumberAvailableSeat() >= findTrain.getNumberAvailableSeat()) {
                listF.add(T);
            }
        }
        for (Train T : listA) {
            if (T.getNumberAvailableSeat() >= findTrain.getNumberAvailableSeat()) {
                listF.add(T);
            }
        }
        return listF;
    }

    //on recupere un train grâce à son id
    @GetMapping("/train/{id}")
    public Train oneTrain(@PathVariable Long id){
        return repository.findById(id).orElseThrow(() -> new LoadDatabase.TrainNotFoundException(id));
    }

    //on crée un train
    @PostMapping("/train/new")
    Train createTrain(@RequestBody Train newTrain){
        return repository.save(newTrain);
    }

    //on modifie un train
    @PutMapping("/train/{id}")
    public Train replaceTrain(@RequestBody Train newTrain, @PathVariable Long id){
        return repository.findById(id)
                .map(train -> {
                    train.setNumberAvailableSeat(newTrain.getNumberAvailableSeat());
                    return repository.save(train);
                })
                .orElseGet(() -> {
                    return repository.save(newTrain);
                });
    }

}
