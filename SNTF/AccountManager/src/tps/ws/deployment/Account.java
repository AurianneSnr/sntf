package tps.ws.deployment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tps.ws.deployment.Model.Personne;
import tps.ws.deployment.Model.Train;
import tps.ws.deployment.Services.*;
import tps.ws.deployment.Services.ServicePersonne;
import tps.ws.deployment.Services.ServiceReservation;
import tps.ws.deployment.Services.ServiceTrain;

public class Account {
	

	public String authentificate(String username, String mdp){
		List<Personne>allUser= new ArrayList<>();
		Personne personne1= new Personne("lola","mdp","Delaroche","Lolita",1);
		Personne personne2= new Personne("max","mdp","Louison","JeanMax",2);
		Personne personne3= new Personne("auri","mdp","Sannier","Aurianne",3);
		allUser.add(personne1);
		allUser.add(personne2);
		allUser.add(personne3);
		ServicePersonne service= new ServicePersonne(allUser);
		ServiceReservation serviceR= new ServiceReservation();
		if(service.getPersonne(username, mdp)==null)
		{
		System.out.println("connexion echouee");
		return "Connexion �chou�e";
		}
		else {
			System.out.println("connexion ok");
			//on r�cup�re tous les trains avec l'id de la personne s'�tant connect�e
			String trains= serviceR.executeGetReservation(service.getPersonne(username, mdp).getId());
			return "[Connexion r�ussie, bonjour "+ service.getPersonne(username, mdp).getSurname() + "]    Voici toutes vos r�servations:  "+ trains;
			}		
		}
	
	public String rechercher() {
		List<Train>allTrains=new ArrayList<>();
		ServiceTrain serviceT= new ServiceTrain();
		String all= serviceT.executeGetAllTrains();
		return "[Liste de tous les trains] " + all;
	}
	
	public String reserver(int idTrain,int idPerson, int nbPlaces) {
		ServiceReservation serviceR = new ServiceReservation();
		String r= serviceR.executePostR(idTrain, idPerson, nbPlaces);
		return "La reservation pour l'utilisateur "+ idPerson+ " de " + nbPlaces + " places pour le train "+idTrain+" est confirmee.";
	}
	
	public String rechercherTrain(String departureStation,String returnStation,int nbPlaces,String classeTrain,String dateDeparture) {
		List<Train>allTrains=new ArrayList<>();
		ServiceTrain serviceT= new ServiceTrain();
		String all= serviceT.executeGetWantedTrain(departureStation, returnStation, nbPlaces, classeTrain, dateDeparture);
		return "[Liste de tous les trains] " + all;
	}
}

